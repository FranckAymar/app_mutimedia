package com.example.tp_app_multimedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MenuTelephoner extends AppCompatActivity {


    public EditText editNumero;
    public Button btnRetour;
    public Button btnAppeler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_telephoner);


        editNumero = findViewById(R.id.editNumero);
        btnRetour = findViewById(R.id.btnRetour);
        btnAppeler = findViewById(R.id.btnAppeler);


        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();

            }
        });


        btnAppeler.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + editNumero.getText()));
                startActivity(intent);

            }
        });


    }
}
