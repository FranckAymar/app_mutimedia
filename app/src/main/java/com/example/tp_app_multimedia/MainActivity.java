package com.example.tp_app_multimedia;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public Button btnMenuTelephoner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMenuTelephoner = findViewById(R.id.btn_menu_telephoner);


        btnMenuTelephoner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent mainIntent = new Intent(MainActivity.this, MenuTelephoner.class);
                startActivity(mainIntent);

            }
        });

    }




}
